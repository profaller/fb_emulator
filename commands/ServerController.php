<?php

namespace app\commands;

use yii\console\Controller;
use app\components\SocketServer;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class ServerController extends Controller
{
    public function actionStart($host)
    {
        $loop = \React\EventLoop\Factory::create();

        $socket = new \React\Socket\Server($loop);
        $socket->listen(SocketServer::SERVER_PORT, $host);

        $server = new IoServer(
            new HttpServer(
                new WsServer(
                    new SocketServer($loop)
                )
            ),
            $socket,
            $loop
        );

        $server->run();
    }
}
