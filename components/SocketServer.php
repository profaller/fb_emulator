<?php
namespace app\components;

use app\models\EmulatorParams;
use app\services\Emulator;
use app\services\EmulatorException;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use React\EventLoop\LoopInterface;
use yii\helpers\Json;

class SocketServer implements MessageComponentInterface
{
    const SERVER_PORT = 12999;

    private $loop;

    protected $clients;


    public function __construct(LoopInterface $loop) {
        $this->loop = $loop;
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $request = Json::decode($msg);
        if (!is_array($request)) {
            return;
        }

        parse_str($request['params'], $params);

        if ($request['action'] == 'human/task') {
            $model = new EmulatorParams();

            if ($model->load($params) && $model->validate()) {

                \Yii::$app->params['xheHost'] = $model->xheHost;

                try {

                    $srv = Emulator::create();

                    $this->log($from, "Start emulation...");
                    $srv->init($model->proxy);
                    sleep(4);

                    $this->log($from, "Login begin...");
                    $srv->login($model->fbLogin, $model->fbPassword);
                    $this->log($from, "End login");
                    sleep(4);

                    $this->log($from, "Scroll begin...");
                    $srv->scrollPage();
                    $this->log($from, "End scroll");
                    sleep(2);

                    $this->log($from, "Random like begin...");
                    $srv->randomLike();
                    $this->log($from, "End random like");
                    sleep(2);

                    $this->log($from, "Logout begin...");
                    $srv->logout();
                    $this->log($from, "End logout");

                    $srv->free();
                    $this->log($from, "Complete!");

                    $this->send($from, 'end', 'End emulation');

                } catch (\Exception $e) {
                    $this->send($from, 'error', $e->getMessage());
                }
            }
        }
    }

    public function send($connection, $type, $message = '')
    {
        $this->loop->nextTick(function() use ($connection, $type, $message) {
            $connection->send(Json::encode(['type' => $type, 'message' => $message]));
        });
        $this->loop->tick();
    }

    public function log($connection, $str)
    {
        $this->send($connection, 'log', $str);
    }
    
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
    }
}