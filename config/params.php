<?php

function dbg($var)
{
    while (@ob_end_clean()) {}
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    die();
}

function cdbg($var)
{
    print_r($var);
    echo "\r\n";
}

return [
    'xheHost' => '192.168.56.101:7017',
];
