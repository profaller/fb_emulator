<?php

namespace app\controllers;

use app\components\WebController;
use app\services\Process;
use app\services\ServerService;

class PanelController extends WebController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionStopServer()
    {
        return $this->json(['success' => ServerService::create()->stopServer()]);
    }

    public function actionStartServer()
    {
        $proc = ServerService::create()->startServer();
        return $this->json([
            'success' => $proc->status(),
            'pid' => $proc->getPid(),
            'savedPid' => ServerService::create()->getSavedPid(),
            'ps aux' => (new Process('ps aux | grep server/start'))->start(false),
            'procResult' => $proc->getResult(),
            'host' => \Yii::$app->request->hostName,
        ]);
    }
}
