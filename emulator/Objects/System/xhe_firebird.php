<?php
////////////////////////////////////////// Firebird /////////////////////////////////////////////////////////
class XHEFirebird extends XHEBaseObject
{
	//////////////////////////// SERVICVE FUNCTIONS /////////////////////////////////////////////////////
	// server initialization
	function __construct($server,$password="")
	{    
		$this->server = $server;
		$this->password = $password;
		$this->prefix = "Firebird";
	}	                                                                                                                           
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	// ������ ���� ��
	function set_db($path)
	{
		$params = array( "path" => $path );
		$res = $this->call_boolean(__FUNCTION__,$params);
		return $res;
	}
	// ��������� SQL ������
	function exe_sql($sql)
	{
		$params = array( "sql" => $sql );
		return $this->call_get(__FUNCTION__,$params);
	}
	// ������� �������
	function create_table($name_table,$arr_names_cells,$arr_types_cells)
	{			  
		$params = array( "name_table" => $name_table , "arr_names_cells" => $arr_names_cells , "arr_types_cells" => $arr_types_cells );
		return $this->call_get(__FUNCTION__,$params);
	}
	// �������� ������
	function insert_record($name_table,$fields,$values)
	{			  
		$params = array( "name_table" => $name_table , "fields" => $fields , "values" => $values );
		return $this->call_get(__FUNCTION__,$params);
	}               

	/////////////////////////////////////////////////////////////////////////////////////////////////////

	// �������� ������
	function get_record($name_table,$name_colums,$where)
	{			  
		$params = array( "name_table" => $name_table , "name_colums" => $name_colums , "where" => $where );
		return $this->call_get(__FUNCTION__,$params);
	}               
	// �������� ���������� �����
	function get_count_rows($name_table)
	{			  
		$params = array( "name_table" => $name_table );
		return $this->call_get(__FUNCTION__,$params);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
};	
?>