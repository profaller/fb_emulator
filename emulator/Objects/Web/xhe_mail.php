<?php
/////////////////////////////////////////// Mail ////////////////////////////////////////////////////
class XHEMail extends XHEBaseObject
{
	/////////////////////////// SERVICVE FUNCTIONS //////////////////////////////////////////////
	// server initialization
	function __construct($server,$password="")
	{    
		$this->server = $server;
		$this->password = $password;
		$this->prefix = "Mail";
	}
	////////////////////////////////////////////////////////////////////////////////////////////
	  	
	////////////////////////////////////////////////////////////////////////////////////////////

	// ���������� � �������� ����� �� POP3
        function pop3_connect($host,$user,$login,$port=110)
	{
	   	$params = array( "host" => $host , "user" => $user , "login" => $login , "port" => $port );
	    	return $this->call_boolean(__FUNCTION__,$params);
	}
	// ������������� �� POP3 �������
        function pop3_disconnect()
	{
	   	$params = array( );
	    	return $this->call_boolean(__FUNCTION__,$params);
	}

	// ������ ������� pop3 ����������
        function set_pop3_timeout($time_out)
	{
	   	$params = array( "time_out" => $time_out  );
	    	return $this->call_boolean(__FUNCTION__,$params);
	}
	// �������� ������� pop3 ����������
        function get_pop3_timeout()
	{
	   	$params = array( );
	    	return $this->call_get(__FUNCTION__,$params);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// �������� ����� ����� �����
        function get_total_count_of_mails()
	{
	   	$params = array( );
	    	return $this->call_get(__FUNCTION__,$params);
	}
	// �������� ����� ������ �����
        function get_total_size_of_mails()
	{
	   	$params = array( );
	    	return $this->call_get(__FUNCTION__,$params);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// �������� ��������� ������ � �������� ������� (��� �������� � �������)
        function get_message_header_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}
	// �������� ������ ������ �� ��� ������ (��� �������� � �������)
        function get_message_size_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}

	// �������� ID ������ �� ��� ������ (��� �������� � �������)
        function get_message_id_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}
	// �������� ����������� ������ �� ��� ������ (��� �������� � �������)
        function get_message_from_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}
	// �������� ���� ������ �� ��� ������ (��� �������� � �������)
        function get_message_date_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}
	// �������� ���� ������ �� ��� ������ (��� �������� � �������)
        function get_message_subject_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// �������� ������ � �������� ������� (� ��������� � �������)
        function retrieve_message_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_get(__FUNCTION__,$params);
	}
	// ������� ������� � �������� �������
        function delete_message_by_number($number)
	{
	   	$params = array( "number" => $number  );
	    	return $this->call_boolean(__FUNCTION__,$params);
	}
	// ����� ������ � �������� �������, ����� � ��� ������ � �������� ������� � ������ �� ���
        function find_and_navigate_on_link_by_number($mail_num, $link_num)
	{
	   	$params = array( "mail_num" => $mail_num , "link_num" => $link_num );
	    	return $this->call_boolean(__FUNCTION__,$params);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// ���������� � smtp �������� (���� �������� -   NoLogin=0, CramMD5=1, Auth Login=2, Login Plain=3)
        function smtp_connect($host,$user,$login,$port=25,$type=0)
	{
	   	$params = array( "host" => $host , "user" => $user , "login" => $login , "port" => $port ,"type" => $type );
	    	$res = $this->call_get(__FUNCTION__,$params);
		if ($res=="true")
			return true;
		else
			return $res;
	}
	// ������������ �� smtp �������
        function smtp_disconnect()
	{
	   	$params = array( );
	    	$res = $this->call_get(__FUNCTION__,$params);
		if ($res=="true")
			return true;
		else
			return $res;
	}

	// ������ smtp �������
        function set_smtp_timeout($time_out)
	{
	   	$params = array( "time_out" => $time_out );
	    	return $this->call_boolean(__FUNCTION__,$params);
	}
	// �������� smtp �������
        function get_smtp_timeout()
	{
	   	$params = array( );
	    	return $this->call_get(__FUNCTION__,$params);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// ������� ���������� � ���� ������
        function send_text_message($text,$to,$from="",$subject="")
	{
	   	$params = array( "text" => $text , "to" => $to , "from" => $from , "subject" => $subject );
	    	$res = $this->call_get(__FUNCTION__,$params);
		if ($res=="true")
			return true;
		else
			return $res;
	}
	// ������� ���������� � ���� html
        function send_html_message($html,$to,$from="",$subject="")
	{
	   	$params = array( "html" => $html , "to" => $to , "from" => $from , "subject" => $subject );
	    	$res = $this->call_get(__FUNCTION__,$params);
		if ($res=="true")
			return true;
		else
			return $res;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
};	
?>