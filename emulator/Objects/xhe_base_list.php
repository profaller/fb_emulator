<?php

class XHEBaseList implements ArrayAccess
{
	/////////////////////////////////////// SERVICE VARIABLES ///////////////////////////////////////////
	// inner number
	var $inner_numbers;
	var $elements=array();
	// server address and port
	var $server;
	// server password
	var $password;
	/////////////////////////////////////// SERVICE FUNCTIONS ///////////////////////////////////////////
	// server initialization
	function __construct($inner_numbers,$server,$password="")
	{    
		$this->inner_number = $inner_numbers;
		$this->server = $server;
		$this->password = $password;
	}

        /////////////////////////////////////////////////////////////////////////////////////////////////////

    	public function __call($name, $arguments) 
	{
		$res=array();
		for ($i=0;$i<count($this->elements);$i++) 
			$res[$i]=$this->elements[$i]->$name($arguments);
		return $res;
	}
        /////////////////////////////////////////////////////////////////////////////////////////////////////

	public function offsetGet($name) 
	{ 
        	if (is_null($name)) 
            		return $this->elements;
        	else 
	            	return $this->elements[$name];
	}
	// �������� []
	public function offsetSet ($name, $value) 
	{
        	if (is_null($name)) 
            		$this->elements[] = $value;
        	else 
	            	$this->elements[$name] = $value;
	}
	public function offsetExists($name) 
	{ 
		if (is_null($name)) 
			return is_null($this->elements);
		else
			return is_null($this->elements[$name]);
        } 
        public function offsetUnset($name) 
	{ 
        } 
        // �������� ����� ���������
   	function get_count()
   	{
		return count ($this->elements);
   	}
        // �������� �������
   	function get($index)
   	{
		if ($index<count($this->elements))
			return $this->elements[$index];
		else
			return false;
   	}
        /////////////////////////////////////////////////////////////////////////////////////////////////////
}

?>