<?php
/////////////////////////////////////////////////////////////////// XHE WRAPER /////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////// Class's


use app\services\XHEApp;

if (!defined("___XHE___"))
{

// ������� ����� ��� ����
include("Objects/xhe_base.php");
// ������� ������
include("Objects/xhe_base_list.php");

// ������� DOM
include("Objects/DOM/xhe_base_dom.php");
include("Objects/DOM/xhe_base_visual_dom.php");

// ��������� �������� �������
include("Tools/file_tools.php");
include("Tools/folder_tools.php");
include("Tools/harvest_tools.php");
include("Tools/string_tools.php");
include("Tools/update_tools.php");

// ��� ������������� � ����������� ��������
include("Objects/DOM/Compatible/xhe_anchor_compatible.php");
include("Objects/DOM/Compatible/xhe_body_compatible.php");
include("Objects/DOM/Compatible/xhe_button_compatible.php");
include("Objects/DOM/Compatible/xhe_checkbutton_compatible.php");
include("Objects/DOM/Compatible/xhe_element_compatible.php");
include("Objects/DOM/Compatible/xhe_form_compatible.php");
include("Objects/DOM/Compatible/xhe_frame_compatible.php");
include("Objects/DOM/Compatible/xhe_image_compatible.php");
include("Objects/DOM/Compatible/xhe_input_compatible.php");
include("Objects/DOM/Compatible/xhe_inputbutton_compatible.php");
include("Objects/DOM/Compatible/xhe_inputfile_compatible.php");
include("Objects/DOM/Compatible/xhe_inputimage_compatible.php");
include("Objects/DOM/Compatible/xhe_radiobox_compatible.php");
include("Objects/DOM/Compatible/xhe_scriptelement_compatible.php");
include("Objects/DOM/Compatible/xhe_selectelement_compatible.php");
include("Objects/DOM/Compatible/xhe_table_compatible.php");
include("Objects/DOM/Compatible/xhe_textarea_compatible.php");
include("Objects/DOM/Compatible/xhe_interface_compatible.php");
include("Objects/Window/Compatible/xhe_windowinterface_compatible.php");
include("Objects/System/Compatible/xhe_folder_compatible.php");
include("Objects/System/Compatible/xhe_file_os_compatible.php");
include("Objects/Web/Compatible/xhe_ftp_compatible.php");
include("Objects/System/Compatible/xhe_keyboard_compatible.php");
include("Objects/Web/Compatible/xhe_webpage_compatible.php");
include("Objects/Web/Compatible/xhe_browser_compatible.php");

// DOM (output)
include("Objects/DOM/xhe_anchor.php");
include("Objects/DOM/xhe_image.php");
include("Objects/DOM/xhe_inputbutton.php");
include("Objects/DOM/xhe_button.php");
include("Objects/DOM/xhe_div.php");
include("Objects/DOM/xhe_span.php");
include("Objects/DOM/xhe_label.php");
include("Objects/DOM/xhe_flash.php");
include("Objects/DOM/xhe_th.php");
include("Objects/DOM/xhe_tr.php");
include("Objects/DOM/xhe_td.php");
include("Objects/DOM/xhe_style.php");
include("Objects/DOM/xhe_h.php");
include("Objects/DOM/xhe_selectelement.php");
include("Objects/DOM/xhe_frame.php");
include("Objects/DOM/xhe_form.php");
include("Objects/DOM/xhe_scriptelement.php");
//include("Objects/DOM/xhe_canvas.php");

// DOM (input)
include("Objects/DOM/xhe_input.php");
include("Objects/DOM/xhe_hiddeninput.php");
include("Objects/DOM/xhe_inputfile.php");
include("Objects/DOM/xhe_textarea.php");
include("Objects/DOM/xhe_checkbutton.php");
include("Objects/DOM/xhe_radiobox.php");
include("Objects/DOM/xhe_table.php");
include("Objects/DOM/xhe_body.php");
include("Objects/DOM/xhe_inputimage.php");
include("Objects/DOM/xhe_element.php");
include("Objects/DOM/xhe_embed.php");
include("Objects/DOM/xhe_object.php");
include("Objects/DOM/xhe_b.php");
include("Objects/DOM/xhe_blockquote.php");
include("Objects/DOM/xhe_code.php");
include("Objects/DOM/xhe_html.php");
include("Objects/DOM/xhe_i.php");
include("Objects/DOM/xhe_li.php");
include("Objects/DOM/xhe_option.php");
include("Objects/DOM/xhe_meta.php");
include("Objects/DOM/xhe_s.php");
include("Objects/DOM/xhe_strong.php");
include("Objects/DOM/xhe_p.php");
include("Objects/DOM/xhe_pre.php");
include("Objects/DOM/xhe_u.php");
include("Objects/DOM/xhe_head.php");
include("Objects/DOM/xhe_canvas.php");


include("Objects/DOM/xhe_interface.php");
include("Objects/DOM/xhe_interfaces.php");

// System
include("Objects/System/xhe_mouse.php");
include("Objects/System/xhe_sound.php");
include("Objects/System/xhe_keyboard.php");
include("Objects/System/xhe_textfile.php");
include("Objects/System/xhe_file.php");
include("Objects/System/xhe_clipboard.php");
include("Objects/System/xhe_folder.php");
//include("Objects/System/xhe_excel.php");
include("Objects/System/xhe_msword.php");
include("Objects/System/xhe_firebird.php");
//include("Objects/System/xhe_mysql.php");

// Web
include("Objects/xhe_base_captcha_1.php");
include("Objects/Web/xhe_anticaptcha.php");
include("Objects/Web/xhe_rucaptcha.php");
include("Objects/Web/xhe_ripcaptcha.php");
include("Objects/Web/xhe_evecaptcha.php");
include("Objects/Web/xhe_bypasscaptcha.php");
include("Objects/Web/xhe_captcha24.php");
include("Objects/Web/xhe_browser.php");
include("Objects/Web/xhe_captchabot.php");
include("Objects/Web/xhe_webpage.php");
include("Objects/Web/xhe_seo.php");
include("Objects/Web/xhe_raw.php");
include("Objects/Web/xhe_connection.php");
include("Objects/Web/xhe_mail.php");
include("Objects/Web/xhe_ftp.php");
include("Objects/Web/xhe_submitter.php");
include("Objects/Web/xhe_proxycheker.php");

// Window
include("Objects/Window/Compatible/xhe_application_compatible.php");
include("Objects/Window/Compatible/xhe_window_compatible.php");
include("Objects/Window/xhe_application.php");
include("Objects/Window/xhe_debug.php");
include("Objects/Window/xhe_scheduler.php");
include("Objects/Window/xhe_window_interface.php");
include("Objects/Window/xhe_window_interfaces.php");
include("Objects/Window/xhe_windowsshell.php");
include("Objects/Window/xhe_window.php");

/////////////////////////////////////////////////////////////////////////// Objects

// ������������ PHP ����� ����� (������������ ��� ������ ����� STDIN � �������)
if (!isset($PHP_Use_Trought_Shell))
	$PHP_Use_Trought_Shell=true; 
if (empty($xhe_host) or $xhe_host=="")
	$xhe_host ="127.0.0.1:7010"; 
// ������� ��������� ����
/*$real_port="";
if ($PHP_Use_Trought_Shell)
	$real_port=trim(fgets(STDIN));

// XWeb human emulator host
if (empty($xhe_host) or $xhe_host=="")
{
  if ($real_port=="")
  $xhe_host ="127.0.0.1:8030"; 
  else
	$xhe_host ="127.0.0.1:".$real_port; 
}
else
{
  if ($real_port!="")
	$xhe_host ="127.0.0.1:".$real_port; 	
}
echo $xhe_host;*/

// XWeb human emulator password
if (empty($server_password) or $server_password=="")
  $server_password="";

$srv = XHEApp::create();

$srv->anticapcha = $anticapcha =  new XHEAnticapcha("antigate.com");

// capcha testing services
$srv->anticaptcha = $anticaptcha =  new XHEAnticapcha("antigate.com");
$srv->captcha24 = $captcha24 =  new XHECaptcha24("captcha24.com");
$srv->rucaptcha = $rucaptcha =  new XHERucapcha("rucaptcha.com");
$srv->ripcaptcha = $ripcaptcha =  new XHERipcaptcha("ripcaptcha.com");
$srv->evecaptcha = $evecaptcha =  new XHEEvecaptcha("eve.cm");
$srv->bypasscaptcha = $bypasscaptcha =  new XHEBypasscaptcha();
$srv->captchabot = $captchabot =  new XHECaptchabot();

// create Window objects
$srv->app = $app =  new XHEApplication($xhe_host,$server_password);
$srv->windows = $windows =  new XHEWindowsShell($xhe_host,$server_password);
$srv->scheduler = $scheduler =  new XHEScheduler($xhe_host,$server_password);
$srv->window = $window =  new XHEWindow($xhe_host,$server_password);
$srv->mouse = $mouse =  new XHEMouse($xhe_host,$server_password);

// create System objects
$srv->sound = $sound =  new XHESound($xhe_host,$server_password);
$srv->debug = $debug =  new XHEDebug($xhe_host,$server_password);
$srv->keyboard = $keyboard =  new XHEKeyboard($xhe_host,$server_password);
$srv->clipboard = $clipboard =  new XHEClipboard($xhe_host,$server_password);
$srv->textfile = $textfile =  new XHETextFile($xhe_host,$server_password);
$srv->file_os = $file_os =  new XHEFile_os($xhe_host,$server_password);
$srv->folder = $folder =  new XHEFolder($xhe_host,$server_password);
$srv->msword = $msword =  new XHEMsWord($xhe_host,$server_password);
$srv->firebird = $firebird =  new XHEFirebird($xhe_host,$server_password);
//$srv->excel = $excel =  new XHEExcel($xhe_host,$server_password);
//$srv->mysql = $mysql =  new XHEMySQL($xhe_host,$server_password);

// create Web objects
$srv->browser = $browser =  new XHEBrowser($xhe_host,$server_password);
$srv->webpage = $webpage =  new XHEWebPage($xhe_host,$server_password);
$srv->raw = $raw =  new XHERaw($xhe_host,$server_password);
$srv->seo = $seo =  new XHESEO($xhe_host,$server_password);
$srv->connection = $connection =  new XHEConnection($xhe_host,$server_password);
$srv->mail = $mail =  new XHEMail($xhe_host,$server_password);
$srv->ftp = $ftp =  new XHEFTP($xhe_host,$server_password);
$srv->submitter = $submitter =  new XHESubmitter($xhe_host,$server_password);
$srv->proxycheker = $proxycheker =  new XHEProxyCheker($xhe_host,$server_password);

// create Dom (container) object
$srv->frame = $frame =  new XHEFrame($xhe_host,$server_password);
$srv->form = $form =  new XHEForm($xhe_host,$server_password);
$srv->body = $body =  new XHEBody($xhe_host,$server_password);

// create Dom (output) objects
$srv->anchor = $anchor =  new XHEAnchor($xhe_host,$server_password);
$srv->image = $image =  new XHEImage($xhe_host,$server_password);
$srv->button = $button =  new XHEInputButton($xhe_host,$server_password);
$srv->div = $div =  new XHEDiv($xhe_host,$server_password);
$srv->span = $span =  new XHESpan($xhe_host,$server_password);
$srv->label = $label =  new XHELabel($xhe_host,$server_password);
$srv->td = $td =  new XHETD($xhe_host,$server_password);
$srv->tr = $tr =  new XHETR($xhe_host,$server_password);
$srv->th = $th =  new XHETH($xhe_host,$server_password);
$srv->style = $style =  new XHEStyle($xhe_host,$server_password);
$srv->btn = $btn =  new XHEButton($xhe_host,$server_password);
$srv->listbox = $listbox =  new XHESelectElement($xhe_host,$server_password);
$srv->script = $script =  new XHEScriptElement($xhe_host,$server_password);
$srv->table = $table =  new XHETable($xhe_host,$server_password);
//$srv->canvas = $canvas =  new XHECanvas($xhe_host,$server_password);
$srv->h1 = $h1 =  new XHEH($xhe_host,1,$server_password);
$srv->h2 = $h2 =  new XHEH($xhe_host,2,$server_password);
$srv->h3 = $h3 =  new XHEH($xhe_host,3,$server_password);
$srv->h4 = $h4 =  new XHEH($xhe_host,4,$server_password);
$srv->h5 = $h5 =  new XHEH($xhe_host,5,$server_password);
$srv->h6 = $h6 =  new XHEH($xhe_host,6,$server_password);

$srv->b = $b =  new XHEB($xhe_host,$server_password);
$srv->blockquote = $blockquote =  new XHEBlockquote($xhe_host,$server_password);
$srv->code = $code =  new XHECode($xhe_host,$server_password);
$srv->html = $html =  new XHEHtml($xhe_host,$server_password);
$srv->i = $i =  new XHEI($xhe_host,$server_password);
$srv->li = $li =  new XHELi($xhe_host,$server_password);
$srv->meta = $meta =  new XHEMeta($xhe_host,$server_password);
$srv->option = $option =  new XHEOption($xhe_host,$server_password);
$srv->p = $p =  new XHEP($xhe_host,$server_password);
$srv->pre = $pre =  new XHEPre($xhe_host,$server_password);
$srv->s = $s =  new XHES($xhe_host,$server_password);
$srv->strong = $strong =  new XHEStrong($xhe_host,$server_password);
$srv->u = $u =  new XHEU($xhe_host,$server_password);
$srv->head = $head =  new XHEHead($xhe_host,$server_password);
$srv->canvas = $canvas =  new XHECanvas($xhe_host,$server_password);

// create Dom (input) objects
$srv->input = $input =  new XHEInput($xhe_host,$server_password);
$srv->hiddeninput = $hiddeninput =  new XHEHiddenInput($xhe_host,$server_password);
$srv->inputfile = $inputfile =  new XHEInputFile($xhe_host,$server_password);
$srv->textarea = $textarea =  new XHETextArea($xhe_host,$server_password);
$srv->checkbox = $checkbox =  new XHECheckButton($xhe_host,$server_password);
$srv->radiobox = $radiobox =  new XHERadioButton($xhe_host,$server_password);
$srv->inputimage = $inputimage =  new XHEInputImage($xhe_host,$server_password);
$srv->element = $element =  new XHEElement($xhe_host,$server_password);
$srv->embed = $embed =  new XHEEmbed($xhe_host,$server_password);
$srv->object = $object =  new XHEObject($xhe_host,$server_password);
$srv->flash = $flash =  new XHEFlash($xhe_host,$server_password);

// for static call
include("xweb_workspace_dom.php");
include("xweb_workspace_system.php");
include("xweb_workspace_web.php");
include("xweb_workspace_window.php");

// run options
$bClosePHPIfNotConnected=false; // ��������� PHP ���� ��� ���������� � �������
$bWarningPHPIfNotConnected=true; // ������������� � ���� ������� ���� ��� ���������� � �������
$Wait_Try_Navigate_Second=15; // ������� ������ ����� ���������� ��������� 
$Wait_Try_Navigate_Count=1; // ������� ��� �������� ����������� ���������
$bUTF8Ver=false; // ������ � �� ������ ������ ������
$bWaitElementExistBeforeAction=false; // ��������� ���� �� �������� ������� �� �������� ����� ���������
$iSecondsWaitElementExistBeforeAction=15; // ������� ������ ����� ��������� ��������

// log option
$bShowInfoByPHPConfiguration=false;
if ($bShowInfoByPHPConfiguration)
{
  echo "==============&nbsp;&nbsp;&nbsp;&nbsp;��������������� ���������� � ������������ PHP �������&nbsp;&nbsp;&nbsp;&nbsp;================<br><br>";
  if ($bClosePHPIfNotConnected)                                                      
    echo "���������� ����� ������������� � ��������� ������, ���� ��� ����� � Human Emulator'��<br>";
  if ($bWarningPHPIfNotConnected)                                                      
    echo "������������� � ���� ������� � ������ ����� � Human Emulator'��<br>";
  echo "������������ ����� ��������� ���������� ��������� : $Wait_Try_Navigate_Second ������ <br>";
  echo "������������ ���������� ������� ����������� ��������� : $Wait_Try_Navigate_Count ���(�) <br>";
  if ($PHP_Use_Trought_Shell)                                                      
    echo "PHP ������� ����������� �� ��������<br>";
  else
    echo "PHP ������� ����������� �� ���������� ������<br>";
  if ($bUTF8Ver)                                                      
    echo "������������ UNICODE ������ ������<br>";
  if ($bWaitElementExistBeforeAction)                                                      
  {
    echo "���������� ����� �������� ��������� �������� ����� ����������� �������� � ���<br>";
    echo "������������ ����� �������� ��������� �������� ����� ����������� �������� � ��� : $iSecondsWaitElementExistBeforeAction ������<br>";
  }
  echo "<br>================================================================================<br><br>";
}

// defines
define ("___XHE___", "DEFINED");
// default timeout for keyboard input (milliseconds)
define ("INPUT_TIME", "100");
// default timeout for execute command (seconds)
XHEBaseObject::$COMMAND_TIME=100;
// default count of try execute command
XHEBaseObject::$COMMAND_TRY_COUNT=3;

}
////////////////////////////////////////////////////////////////////////////////////
?>