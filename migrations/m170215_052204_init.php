<?php

use app\models\User;
use yii\db\Migration;
use yii\rbac\Role;

class m170215_052204_init extends Migration
{
    public function up()
    {
        $migration = new \yii\console\controllers\MigrateController('migrate', Yii::$app);
        $migration->runAction('up', ['migrationPath' => '@yii/rbac/migrations', 'interactive' => false]);

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'role' => $this->string(10),
            'login' => $this->string()->notNull()->unique(),
            'email' => $this->string(),
            'name' => $this->string(),
            'password' => $this->string()->notNull(),
            'token' => $this->string(),
            'auth_key' => $this->string()->notNull(),
            'created_at' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
        ]);

        $auth = Yii::$app->authManager;

        $role = $auth->createRole('admin');
        $auth->add($role);


        $model = new User();
        $model->login = 'admin';
        $model->password = 'ghbrjk';
        $model->role = 'admin';
        if ($model->save()) {
            $auth->assign($role, $model->id);
        };
    }

    public function down()
    {
        echo "m170215_052204_tbl_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
