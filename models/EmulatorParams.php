<?php

namespace app\models;

use yii\base\Model;



class EmulatorParams extends Model
{
    public $xheHost;

    public $fbLogin;

    public $fbPassword;

    public $proxy;


    public function bindTestParams()
    {
        $this->xheHost = \Yii::$app->request->hostName == 'fbhuman.dev' ? '192.168.56.101:7017' : '85.114.11.38:47017';

        /*$this->xheHost = '85.114.11.38:47017';
        $this->fbLogin = 'zhdanova.iulya2017@yandex.ru';
        $this->fbPassword = 'DY901XH2';
        $this->proxy = '185.128.212.2:9450@user283:39dkfjfj';


        $this->xheHost = '192.168.56.101:7017';
        $this->fbLogin = 'profaller@bk.ru';
        $this->fbPassword = 'digitaL9';
        $this->proxy = '185.128.212.2:9450@user283:39dkfjfj';*/
    }

    public function rules()
    {
        return [
            [['xheHost', 'fbLogin', 'fbPassword', 'proxy'], 'required'],
        ];
    }
}