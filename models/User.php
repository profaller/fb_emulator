<?php

namespace app\models;

use app\enums\Role;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $role
 * @property string $login
 * @property string $email
 * @property string $name
 * @property string $password
 * @property string $token
 * @property string $auth_key
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['login', 'email'], 'trim'],
            [['login', 'role'], 'required'],

            [['password'], 'required', 'on' => ['insert']],
            [['password'], 'string', 'min' => 3],

            ['login', 'unique', 'on' => 'default'],
            ['email', 'email'],
            [['email', 'login', 'name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'login' => 'Login',
            'email' => 'Email',
            'name' => 'Name',
            'password' => 'Password',
            'token' => 'Token',
            'auth_key' => 'Auth Key',
            'created_at' => 'Created At',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $this->token = Yii::$app->getSecurity()->generateRandomString();
        } else if (!empty($this->password) && $this->password != $this->oldAttributes['password']) {
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $this->token = Yii::$app->getSecurity()->generateRandomString();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $attr)
    {
        if ($insert) {
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($this->role), $this->id);
        }
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }
}
