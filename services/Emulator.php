<?php

namespace app\services;

use app\models\User;
use app\models\Users;


require \Yii::getAlias('@app') . '/services/Finder.php';


class Emulator
{
    private static  $_instance = null;


    private final function __construct()
    {

    }

    public static function create()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function init($proxy)
    {
        $app = XHEApp::create();

        if (!$app->browser->recreate()) {
            throw new EmulatorException('Cant create XHE browser.');
        }
        \XHEBaseObject::$COMMAND_TIME = 130;

        $app->browser->set_home_page('http://ya.ru');
        $app->browser->clear_cache();
        $app->browser->clear_cookies('', true, true);

        $app->browser->enable_proxy('', $proxy);

        $app->browser->navigate("http://www.facebook.com");
    }

    public function free()
    {
        XHEApp::create()->app->quit();
    }

    public function login($username, $password)
    {
        $loginForm = _xheQuery('#login_form');

        $input = $loginForm->find('[name=email]');
        $input->mouse_move(rand(15, 40), rand(3, 10), 1, 3);
        $input->click();
        $input->set_value($username);

        $input = $loginForm->find('[name=pass]');
        $input->mouse_move(rand(15, 40), rand(3, 10), 0.5, 3);
        $input->click();
        $input->set_value($password);

        $btn = $loginForm->find('[type=submit]');
        $btn->mouse_move(rand(10, 20), rand(3, 10), 0.3, 3);
        $btn->click();

        sleep(5);

        // check additional auth popup
        $authPopup = _xheQuery('#u_0_10');
        if (count($authPopup)) {
            $input = $authPopup->find('#pass');
            $input->mouse_move(rand(15, 40), rand(3, 10), 2, 3);
            $input->mouse_click();
            $input->set_value($password);

            $btn = $authPopup->find('[type=submit]');
            $btn->mouse_move(rand(10, 30), rand(3, 10), 0.5, 3);
            $btn->mouse_click(rand(10, 30), rand(3, 10));
        };

        sleep(3);

        // is logging successful
        $profileLink = _xheQuery('[data-testid=blue_bar_profile_link]');
        if (count($profileLink) == 0) {
            throw new EmulatorException('Cant login!');
        }

    }

    public function logout()
    {
        $menu = _xheQuery('#pageLoginAnchor');
        $menu->mouse_move(rand(1, 3), rand(1, 3), 1, 5);
        $menu->mouse_click();

        sleep(1);

        $link = _xheQuery('[data-gt~=menu_logout]')->eq(1);
        $link->mouse_move(rand(1, 3), rand(1, 3), 1, 5);
        $link->mouse_click();
    }

    public function randomLike()
    {
        $likeBlocks = _xheQuery('.commentable_item');
        $target = $likeBlocks->eq(rand(0, count($likeBlocks)))->find('.UFILikeLink');
        $target->scroll_to_view(true);
        $target->mouse_move(rand(5, 10), rand(2, 5), 0.5, 3);
        $target->mouse_click();
    }

    public function scrollPage($seconds = 60)
    {
        $app = XHEApp::create();

        $width = $app->browser->get_page_width();
        $height = $app->browser->get_page_height();
        $toX = rand(100, $width / 2);
        $toY = rand(100, $height / 2);
        $app->mouse->move(100, 100, false, 0.5, 5);

        $delay = 0;
        for ($i = 0; $i < $seconds; $i += $delay) {
            $app->mouse->wheel(-rand(3, 6), $toX, $toY);
            $delay = rand(1, 4);
            sleep($delay);
        }
    }
}


class EmulatorException extends \Exception
{

}