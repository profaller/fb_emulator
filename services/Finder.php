<?php


use app\services\XHEApp;

function _xheQuery($selector) {
    return new _finder($selector);
}

class _finderQuery
{
    const ATTR_FIND_MODE_EXACT = 1;
    const ATTR_FIND_MODE_WS_SEPARATED = 2;

    /**
     * @param XHEInterface[] $elements
     * @param $name
     * @param $value
     * @param integer $mode
     * @return array
     */
    public static function findByAttr($elements = null, $name, $value, $mode = self::ATTR_FIND_MODE_EXACT)
    {
        $result = [];

        if ($elements) {
            foreach ($elements as $element) {
                if ($mode == self::ATTR_FIND_MODE_EXACT) {
                    if ($value != $element->get_attribute($name)) {
                        continue;
                    }
                } elseif ($mode == self::ATTR_FIND_MODE_WS_SEPARATED) {
                    if (!preg_match('#\b' . $value . '\b#is', $element->get_attribute($name))) {
                        continue;
                    }
                }


                $result[] = $element;
            }
        } else {
            $app = XHEApp::create();
            $result = $app->element->get_all_by_attribute($name, $value, $mode == self::ATTR_FIND_MODE_EXACT)->elements;
        }

        return $result;
    }
}

/**
 * Class _finder
 *
 * @method get_clone()
 * @method click()
 * @method event($event)
 * @method focus()
 * @method scroll_to_view($start)
 * @method scroll($scroll_action)
 * @method set_value($value)
 * @method set_inner_text($inner_text)
 * @method set_inner_html($inner_html)
 * @method add_attribute($name_atr,$value_atr)
 * @method set_attribute($name_atr,$value_atr)
 * @method remove_attribute($name_atr)
 * @method screenshot($file_path)
 * @method get_tag()
 * @method get_name()
 * @method get_id()
 * @method get_number($object_name="")
 * @method get_inner_text()
 * @method get_inner_html()
 * @method get_value()
 * @method get_href()
 * @method get_src()
 * @method get_alt()
 * @method get_attribute($name_atr)
 * @method get_all_attributes()
 * @method get_all_attributes_values()
 * @method get_all_events()
 * @method is_disabled()
 * @method is_exist()
 * @method is_visibled()
 * @method get_numbers_child($element_type="")
 * @method get_x()
 * @method get_y()
 * @method get_width()
 * @method get_height()
 * @method get_parent()
 * @method add_child($tag,$inner_html)
 * @method insert_before($tag,$inner_html)
 * @method remove()
 * @method get_child_count()
 * @method get_child_by_number($number)
 * @method get_child_by_inner_text($inner_text,$exactly=false)
 * @method get_child_by_inner_html($inner_html,$exactly=false)
 * @method get_child_by_attribute($atr_name,$atr_value,$exactly=true)
 * @method get_all_child_by_inner_text($inner_text,$exactly=false)
 * @method get_all_child_by_inner_html($inner_html,$exactly=false)
 * @method get_all_child_by_attribute($atr_name,$atr_value,$exactly=true)
 * @method get_next($tag="",$type="")
 * @method get_prev($tag="",$type="")
 * @method get_child($tag="",$type="")
 * @method mouse_move($dx=1,$dy=1,$time=0,$tremble=0)
 * @method mouse_click($dx=1,$dy=1)
 * @method mouse_double_click($dx=1,$dy=1)
 * @method mouse_left_down($dx=1,$dy=1)
 * @method mouse_left_up($dx=1,$dy=1)
 * @method mouse_right_click($dx=1,$dy=1)
 * @method mouse_right_down($dx=1,$dy=1)
 * @method mouse_right_up($dx=1,$dy=1)
 * @method send_mouse_move($dx=1,$dy=1,$time=0,$tremble=0,$buttons="")
 * @method send_mouse_click($dx=1,$dy=1)
 * @method send_mouse_double_click($dx=1,$dy=1)
 * @method send_mouse_left_down($dx=1,$dy=1)
 * @method send_mouse_left_up($dx=1,$dy=1)
 * @method send_mouse_right_click($dx=1,$dy=1)
 * @method send_mouse_right_down($dx=1,$dy=1)
 * @method send_mouse_right_up($dx=1,$dy=1)
 * @method send_input($string,$timeout=INPUT_TIME,$inFlash=false,$auto_change=true)
 * @method send_key($key,$is_key=false)
 * @method send_key_down($key,$is_key=false)
 * @method send_key_up($key,$is_key=false)
 * @method input($string,$timeout=INPUT_TIME,$inFlash=false,$auto_change=true)
 * @method !key($code)
 * @method key_down($key)
 * @method key_up($key)
 */
class _finder implements Iterator, Countable
{
    public $position = 0;

    /** @var XHEInterface[] */
    public $collection = [];


    public function __construct($selector = null)
    {
        if ($selector !== null) {
            $this->_find($selector);
        }
    }

    /**
     * @param string $selector
     * @param null|XHEInterface $context
     * @return $this
     */
    public function _find($selector, $context = null)
    {
        $app = XHEApp::create();

        $attr = false;
        $attrValue = false;
        $attrFindMode = _finderQuery::ATTR_FIND_MODE_EXACT;

        if (strpos($selector, '#') === 0) {
            $attr = 'id';
            $attrValue = substr($selector, 1);
        } elseif (strpos($selector, '.') === 0) {
            $attr = 'class';
            $attrValue = substr($selector, 1);
            $attrFindMode = _finderQuery::ATTR_FIND_MODE_WS_SEPARATED;
        } elseif (preg_match('#\[([\w-]+?)(~)?=([\w\d_]+?)\]#is', $selector, $matches)) {
            $attr = $matches[1];
            $attrValue = $matches[3];
            switch ($matches[2]) {
                case '~':
                    $attrFindMode = _finderQuery::ATTR_FIND_MODE_WS_SEPARATED;
            }
        }


        if ($context) {
            $res = $app->element->get_numbers_child_by_number($context->get_number());

            /** @var XHEInterface[] $contextElements */
            $contextElements = [];
            foreach (explode('<br>', $res) as $num) {
                $contextElements[] = $app->element->get_by_number($num);
            }

            if (!$attr) {
                foreach ($contextElements as $element) {
                    if ($selector == strtolower($element->get_tag())) {
                        $this->collection[] = $element;
                    }
                }
            } elseif ($attr == 'class') {
                $this->collection = _finderQuery::findByAttr($contextElements, $attr, $attrValue, $attrFindMode);
            } else {
                $this->collection = _finderQuery::findByAttr($contextElements, $attr, $attrValue, $attrFindMode);
            }

        } else {


            if (!$attr) {
                $xheI = $app->element->get_all_by_tag($selector);
                foreach ($xheI->elements as $element) {
                    $this->collection[] = $element;
                }
            } elseif ($attr == 'id') {
                $this->collection[] = $app->element->get_by_id($attrValue);
            } else {
                $this->collection = _finderQuery::findByAttr(null, $attr, $attrValue, $attrFindMode);
            }
        }

        return $this;
    }

    public function find($selector)
    {
        $finder = new self();
        foreach ($this->collection as $element) {
            $finder->_find($selector, $element);
        }

        return $finder;
    }

    /**
     * @param $index
     * @return $this
     */
    public function eq($index)
    {
        $finder = new self();
        if (isset($this->collection[$index])) {
            $finder->collection[] = $this->collection[$index];
        }

        return $finder;
    }

    public function __call($name, $arguments)
    {
        foreach ($this->collection as $element) {
            return call_user_func_array([$element, $name], $arguments);
        }
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->collection[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->collection[$this->position]);
    }

    public function count() {
        return count($this->collection);
    }
}