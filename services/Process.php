<?php


namespace app\services;


/**
 * An easy way to keep in track of external processes.
 * Ever wanted to execute a process in php, but you still wanted to have somewhat controll of the process ? Well.. This is a way of doing it.
 * @compability: Linux only. (Windows does not work).
 * @author: Peec
 */
class Process
{
    private $pid;

    private $command;

    private $result;

    private $output;


    public function __construct($command = null)
    {
        if ($command) {
            $this->command = $command;
        }
    }

    public function setCommand($command)
    {
        $this->command = $command;
    }

    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    public function getPid()
    {
        return $this->pid;
    }

    public function status()
    {
        $command = 'ps -p ' . $this->pid;
        exec($command, $op);
        if (!isset($op[1])) return false;
        else return true;
    }

    public function start($background = true)
    {
        if ($this->command != '') {
            if ($background) {
                $command = 'nohup ' . $this->command . ' > /dev/null 2>&1 & echo $!';
                exec($command, $this->output, $this->result);
                $this->pid = (int)$this->output[0];
                return true;
            } else {
                $command = 'nohup ' . $this->command;
                exec($command, $this->output, $this->result);
                $this->pid = -1;
                return $this->output;
            }
        }

        return false;
    }

    public function stop()
    {
        $command = 'kill ' . $this->pid;
        exec($command);
        if ($this->status() == false) return true;
        else return false;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function getResult()
    {
        return $this->result;
    }
}