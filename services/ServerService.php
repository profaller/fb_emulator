<?php

namespace app\services;




class ServerService
{
    private static  $_instance = null;


    private final function __construct()
    {

    }

    public static function create()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function startServer()
    {
        $proc = new Process('php ' . \Yii::getAlias('@app') . '/yii server/start 0.0.0.0');

        $path = \Yii::getAlias('@runtime') . '/human.pid';
        if (file_exists($path)) {
            $pid = file_get_contents($path);
            $proc->setPid($pid);

            if (!$proc->status()) {
                $proc->start();
                $pid = $proc->getPid();
            }
        } else {
            $proc->start();
            $pid = $proc->getPid();
        }

        file_put_contents($path, $pid);

        return $proc;
    }

    public function stopServer()
    {
        $proc = new Process();

        $path = \Yii::getAlias('@runtime') . '/human.pid';
        if (file_exists($path)) {
            $pid = file_get_contents($path);
            $proc->setPid($pid);
            $proc->stop();

            return !$proc->status();
        }

        return false;
    }

    public function isServerRun()
    {
        $proc = new Process();

        $path = \Yii::getAlias('@runtime') . '/human.pid';
        if (file_exists($path)) {
            $pid = file_get_contents($path);
            $proc->setPid($pid);

            return $proc->status();
        }

        return false;
    }

    public function getSavedPid()
    {
        $pid = -1;
        $path = \Yii::getAlias('@runtime') . '/human.pid';
        if (file_exists($path)) {
            $pid = file_get_contents($path);
        }

        return $pid;
    }
}