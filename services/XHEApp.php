<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 07.02.17
 * Time: 5:55
 */

namespace app\services;


$xhe_host = \Yii::$app->params['xheHost'];
require(__DIR__ .  "/../emulator/xweb_human_emulator.php");

/**
 * Class XHEApp
 */
class XHEApp
{
    /** @var \XHEAnticapcha */
    public $anticaptcha;
    /** @var \XHECaptcha24 */
    public $captcha24;
    /** @var \XHERucapcha */
    public $rucaptcha;
    /** @var \XHERipcaptcha */
    public $ripcaptcha;
    /** @var \XHEEvecaptcha */
    public $evecaptcha;
    /** @var \XHEBypasscaptcha */
    public $bypasscaptcha;
    /** @var \XHECaptchabot */
    public $captchabot;
    /** @var \XHEApplication */
    public $app;
    /** @var \XHEWindowsShell */
    public $windows;
    /** @var \XHEScheduler */
    public $scheduler;
    /** @var \XHEWindow */
    public $window;
    /** @var \XHEMouse */
    public $mouse;
    /** @var \XHESound */
    public $sound;
    /** @var \XHEDebug */
    public $debug;
    /** @var \XHEKeyboard */
    public $keyboard;
    /** @var \XHEClipboard */
    public $clipboard;
    /** @var \XHETextFile */
    public $textfile;
    /** @var \XHEFile_os */
    public $file_os;
    /** @var \XHEFolder */
    public $folder;
    /** @var \XHEMsWord */
    public $msword;
    /** @var \XHEFirebird */
    public $firebird;
    /** @var \XHEExcel */
    public $excel;
    /** @var \XHEMySQL */
    public $mysql;
    /** @var \XHEBrowser */
    public $browser;
    /** @var \XHEWebPage */
    public $webpage;
    /** @var \XHERaw */
    public $raw;
    /** @var \XHESEO */
    public $seo;
    /** @var \XHEConnection */
    public $connection;
    /** @var \XHEMail */
    public $mail;
    /** @var \XHEFTP */
    public $ftp;
    /** @var \XHESubmitter */
    public $submitter;
    /** @var \XHEProxyCheker */
    public $proxycheker;
    /** @var \XHEFrame */
    public $frame;
    /** @var \XHEForm */
    public $form;
    /** @var \XHEBody */
    public $body;
    /** @var \XHEAnchor */
    public $anchor;
    /** @var \XHEImage */
    public $image;
    /** @var \XHEInputButton */
    public $button;
    /** @var \XHEDiv */
    public $div;
    /** @var \XHESpan */
    public $span;
    /** @var \XHELabel */
    public $label;
    /** @var \XHETD */
    public $td;
    /** @var \XHETR */
    public $tr;
    /** @var \XHETH */
    public $th;
    /** @var \XHEStyle */
    public $style;
    /** @var \XHEButton */
    public $btn;
    /** @var \XHESelectElement */
    public $listbox;
    /** @var \XHEScriptElement */
    public $script;
    /** @var \XHETable */
    public $table;
    /** @var \XHECanvas */
    public $canvas;
    /** @var \XHEH */
    public $h1;
    /** @var \XHEH */
    public $h2;
    /** @var \XHEH */
    public $h3;
    /** @var \XHEH */
    public $h4;
    /** @var \XHEH */
    public $h5;
    /** @var \XHEH */
    public $h6;
    /** @var \XHEB */
    public $b;
    /** @var \XHEBlockquote */
    public $blockquote;
    /** @var \XHECode */
    public $code;
    /** @var \XHEHtml */
    public $html;
    /** @var \XHEI */
    public $i;
    /** @var \XHELi */
    public $li;
    /** @var \XHEMeta */
    public $meta;
    /** @var \XHEOption */
    public $option;
    /** @var \XHEP */
    public $p;
    /** @var \XHEPre */
    public $pre;
    /** @var \XHES */
    public $s;
    /** @var \XHEStrong */
    public $strong;
    /** @var \XHEU */
    public $u;
    /** @var \XHEHead */
    public $head;
    /** @var \XHEInput */
    public $input;
    /** @var \XHEHiddenInput */
    public $hiddeninput;
    /** @var \XHEInputFile */
    public $inputfile;
    /** @var \XHETextArea */
    public $textarea;
    /** @var \XHECheckButton */
    public $checkbox;
    /** @var \XHERadioButton */
    public $radiobox;
    /** @var \XHEInputImage */
    public $inputimage;
    /** @var \XHEElement */
    public $element;
    /** @var \XHEEmbed */
    public $embed;
    /** @var \XHEObject */
    public $object;
    /** @var \XHEFlash */
    public $flash;



    private static  $_instance = null;


    private final function __construct()
    {

    }

    public static function create()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}