<?php

use app\components\SocketServer;
use app\models\EmulatorParams;
use app\services\ServerService;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$model = new EmulatorParams();
$model->bindTestParams();

?>


<div class="panel-index" data-app-controller="panel_startFrom">

    <div data-app-controller="panel_ServerControls" data-is-running="<?= Json::encode(ServerService::create()->isServerRun())?>">
        <button
                data-href="<?= Url::toRoute(['panel/start-server']) ?>"
                data-disable-text="Server running..."
                data-normal-text="Run"
                class="btn btn-success js-start">Start</button>

        <button data-href="<?= Url::toRoute(['panel/stop-server']) ?>" class="btn btn-danger js-stop">Stop</button>
    </div>
    <br>

    <?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['/panel/start']),
        'options' => [
                'class' => 'form form-inline'
        ]
    ])?>

        <?= $form->field($model, 'xheHost')?>
        <?= $form->field($model, 'fbLogin')?>
        <?= $form->field($model, 'fbPassword')?>
        <?= $form->field($model, 'proxy')?>

        <div class="form-group" style="vertical-align: top">
            <?= Html::submitButton('Begin emulation', [
                'class' => 'btn btn-primary',
                'data-loading-text' => 'Running...'
            ])?>
        </div>

    <?php ActiveForm::end()?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Log</div>
        </div>
        <div class="panel-body js-log">

        </div>
    </div>

    <script>
        window.serverUri = '<?= 'ws://' . \Yii::$app->request->hostName . ':' . SocketServer::SERVER_PORT ?>';
    </script>

</div>
