(function($){

    App.Widgets.panel_ServerControls = can.Control.extend(
        {
            pluginName: 'panel_ServerControls'
        },
        {
            init: function () {
                this.start = this.options.start =  this.element.find('.js-start');
                this.stop = this.options.stop =  this.element.find('.js-stop');
                this.on();

                this.isRun = this.element.data('is-running');

                this.updateState();
            },

            '{start} click': function (btn, e) {
                e.preventDefault();

                $.ajax({
                    url: btn.data('href')
                }).done(this.proxy(function (data) {
                    if (data.success) {
                        this.isRun = true;
                        this.updateState();
                    } else {
                        console.log('cant start');
                    }
                }));
            },

            '{stop} click': function (btn, e) {
                e.preventDefault();

                $.ajax({
                    url: btn.data('href')
                }).done(this.proxy(function (data) {
                    if (data.success) {
                        this.isRun = false;
                        this.updateState();
                    } else {
                        console.log('cant stop');
                    }
                }));
            },

            updateState: function () {
                if (this.isRun) {
                    this.start.prop('disabled', true);
                    this.start.text(this.start.data('disable-text'));
                } else {
                    this.start.prop('disabled', false);
                    this.start.text(this.start.data('normal-text'));
                }
            }
        }
    );

    App.Widgets.panel_startFrom = can.Control.extend(
        {
            pluginName: 'panel_startFrom'
        },
        {
            init: function () {
                this.form = this.options.form =  this.element.find('form');
                this.submit = this.options.submit =  this.element.find('[type=submit]');
                this.log = this.options.log =  this.element.find('.js-log');
                this.on();

                this.socket = null;
            },

            createSocket: function () {
                this.socket = new WebSocket(window.serverUri);

                this.socket.onerror = this.proxy(function(e) {
                    console.log(e);
                });
                this.socket.onmessage = this.proxy(function(e) {
                    var data = JSON.parse(e.data);

                    if (data) {
                        if (data.type == 'log') {

                            this.log.append('<div class="log-item">' + data.message + '</div>');

                        } else if (data.type == 'end') {

                            this.submit.button('reset');
                            this.log.append('<div class="log-item">' + data.message + '</div>');

                        } else if (data.type == 'error') {

                            this.log.append('<div class="log-item__error">' + data.message + '</div>');
                            this.submit.button('reset');

                        }
                    }
                });
            },

            startAlgo: function () {
                if (!this.socket) {
                    this.createSocket();
                }

                var timer = setInterval(this.proxy(function () {
                    if (this.socket.readyState == WebSocket.OPEN) {
                        clearInterval(timer);

                        this.socket.send(JSON.stringify({
                            action: 'human/task',
                            params: this.form.serialize()
                        }));
                    }
                }), 1000);
            },

            '{form} submit': function (form, e) {
                e.preventDefault();
            },

            '{submit} click': function (btn, e) {
                if (!App.CtrlInstances.panel_ServerControls.isRun) {
                    e.preventDefault();
                    return;
                }

                this.submit.button('loading');
                setTimeout(this.proxy(function () {
                    if (!this.form.find('.has-error').length) {
                        this.startAlgo();
                    } else {
                        this.submit.button('reset');
                    }
                }), 1000);
            }
        }
    );


}(jQuery));